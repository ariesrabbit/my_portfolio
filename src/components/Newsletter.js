import { useState, useEffect } from "react";
import { Col, Row, Alert } from "react-bootstrap";

export const Newsletter = ({ status, message, onValidated }) => {
  const [email, setEmail] = useState("");

  useEffect(() => {
    if (status === "success") clearFields();
  }, [status]);

  const handleSubmit = (e) => {
    e.preventDefault();
    email &&
      email.indexOf("@") > -1 &&
      onValidated({
        EMAIL: email,
      });
  };

  const clearFields = () => {
    setEmail("");
  };

  return (
    <Col lg={12}>
      <div className="newsletter-bx wow slideInUp">
        <Row>
          <Col lg={12} md={6} xl={5}>
            <h3>
              Download my CV<br></br> & For more info detail
            </h3>
            {status === "sending" && <Alert>Sending...</Alert>}
            {status === "error" && <Alert variant="danger">{message}</Alert>}
            {status === "success" && <Alert variant="success">{message}</Alert>}
          </Col>
          <Col md={6} xl={7}>
            <form>
              <div className="new-email-bx">
                <input disabled placeholder="Click button to download my CV" />

                <a
                  href="https://drive.google.com/file/d/1xE5__pB_aNQRVV1NRXGB8D7ILz77UM3V/view?usp=sharing"
                  target="_blank"
                >
                  Download
                </a>
              </div>
            </form>
          </Col>
        </Row>
      </div>
    </Col>
  );
};
